import 'dart:io';
import 'dart:math';

import 'package:advent2022/campcleanup.dart';
import 'package:advent2022/cargoship.dart';
import 'package:advent2022/command_line_simulator.dart';
import 'package:advent2022/communicator.dart';
import 'package:advent2022/crt_cpu.dart';
import 'package:advent2022/elf.dart';
import 'package:advent2022/rockpaperscissor.dart';
import 'package:advent2022/rope.dart';
import 'package:advent2022/rucksack.dart';
import 'package:advent2022/tree_visibility.dart';

void main() {
  day9();
}

void day1() {
  final lines = File('data/elves.txt').readAsLinesSync();
  final elves = parseElves(lines);
  elves.sort((e1, e2) => e2.totalFood.compareTo(e1.totalFood));
  print('Top elf: ${elves.first}');
  final topThreeElfTotal = elves
      .getRange(0, 3)
      .map((e) => e.totalFood)
      .reduce((total, e) => total + e);
  print('Top 3 Elves total food: $topThreeElfTotal');
}

void day2a() {
  final lines = File('data/rps_strategy_guide.txt').readAsLinesSync();
  final games = parseGame1(lines);
  final totalScore =
      games.map((g) => g.playRound()).reduce((total, game) => total + game);
  print('Your total score: $totalScore');
}

void day2b() {
  final lines = File('data/rps_strategy_guide.txt').readAsLinesSync();
  final games = parseGame2(lines);
  final totalScore =
      games.map((g) => g.playRound()).reduce((total, game) => total + game);
  print('Your total score: $totalScore');
}

void day3() {
  final lines = File('data/rucksacks.txt').readAsLinesSync();
  final rucksacks = lines.map((l) => Rucksack.fromInputString(l)).toList();
  final total =
      rucksacks.map((r) => r.itemsInBothSum).reduce((sum, i) => sum + i);
  print('Duplicate item total: $total');
  final badgeTotal = rucksacks
      .partition(3)
      .map((group) => group.groupBadgeCandidates().first)
      .reduce((sum, badge) => sum + badge);
  print('Badge total: $badgeTotal');
}

void day4() {
  final lines = File('data/camp_pairs.txt').readAsLinesSync();
  final pairs = lines.map((l) => SectionList.fromCsvLine(l)).toList();
  final totallyContaining = pairs
      .where((pair) =>
          pair.first.contains(pair.last) || pair.last.contains(pair.first))
      .toList();
  totallyContaining.forEach(print);
  print('# Containing pairs: ${totallyContaining.length}');
  final overlapping = pairs
      .where((pair) =>
          pair.first.overlaps(pair.last) || pair.last.overlaps(pair.first))
      .toList();
  overlapping.forEach(print);
  print('# Overlapping pairs: ${overlapping.length}');
}

void day5() {
  final lines = File('data/ship_cargo.txt').readAsLinesSync();
  final headerEnd = lines.indexWhere((element) => element.isEmpty);
  final headerLines = lines.sublist(0, headerEnd);
  final moveLines = lines.sublist(headerEnd + 1);
  var ships = parseInitialStacks(headerLines);
  print('Moved with CrateMover 9000');
  for (final moveLine in moveLines) {
    final move = MoveInstruction.fromLine(moveLine);
    executeMoveInstruction(ships, move, false);
  }
  final cm9000tops = ships.map((s) => s.first).join();
  print(cm9000tops);

  print('Moved with CrateMover 9001');
  ships = parseInitialStacks(headerLines);
  for (final moveLine in moveLines) {
    final move = MoveInstruction.fromLine(moveLine);
    executeMoveInstruction(ships, move, true);
  }
  final cm9001tops = ships.map((s) => s.first).join();
  print(cm9001tops);
}

void day6() {
  final stream = File('data/communication_stream.txt').readAsStringSync();
  print('Start of stream: ${findStart(stream, StartType.header)}');
  print('Start of message: ${findStart(stream, StartType.message)}');
}

void day7() {
  final replay = File('data/cli_replay.txt').readAsLinesSync();
  final sim = replayOutputFile(replay);
  var sum = 0;
  for (final dir in sim.allDirs) {
    final size = dir.totalSize();
    if (size <= 100000) {
      sum += size;
    }
  }
  print('Sum of sizes at or under 100,000: $sum');
  const maxFsSize = 70000000;
  const neededFreeSpace = 30000000;
  final currentSize = sim.rootDirectory.totalSize();
  final currentFreeSpace = maxFsSize - currentSize;
  final needToDelete = neededFreeSpace - currentFreeSpace;
  print(needToDelete);
  sim.allDirs.sort((d1, d2) => d1.totalSize().compareTo(d2.totalSize()));
  final firstBiggest =
      sim.allDirs.firstWhere((d) => d.totalSize() >= needToDelete);
  print('${firstBiggest.name} => ${firstBiggest.totalSize()}');
}

void day8() {
  final trees = File('data/trees.txt').readAsLinesSync();
  final grid = TreeGrid.fromLines(trees);
  print('Total visible trees: ${grid.totalVisibleCount()}');
  final scenicScores = grid.calcScenicScoreGrid();
  final maxScore = scenicScores
      .map((row) => row.reduce((maxScore, score) => max(maxScore, score)))
      .reduce((maxScore, rowMaxScore) => max(maxScore, rowMaxScore));
  print('Max Scenic Score: $maxScore');
}

void day9() {
  final moves = File('data/rope_moves.txt').readAsLinesSync();
  //final moves = ['R 5', 'U 8', 'L 8', 'D 3', 'R 17', 'D 10', 'L 25', 'U 20'];
  // final moves = ['R 4', 'U 4', 'L 6'];
  final rope = Rope();
  for (final move in moves) {
    final elements = move.split(RegExp('\\s+'));
    final count = int.parse(elements[1]);
    final direction = elements[0];
    print(move);
    for (var i = 0; i < count; i++) {
      if (direction == 'R') {
        rope.moveHeadRight();
      } else if (direction == 'L') {
        rope.moveHeadLeft();
      } else if (direction == 'U') {
        rope.moveHeadUp();
      } else if (direction == 'D') {
        rope.moveHeadDown();
      } else {
        throw ArgumentError('Unknown direction: $direction');
      }
      // print(drawRope(rope));
      // print('');
    }
  }

  print('Tail: ');
  print(drawRope(rope, asTail: true));

  print('Tail positions count: ${rope.tailHistory.length}');
}

void day10a() {
  final cpu = Cpu();
  File('data/cpu_instructions.txt')
      .readAsLinesSync()
      .map((l) => Instruction.fromLine(l))
      .forEach((i) => cpu.addInstruction(i));
  final wantedCycles = [20, 60, 100, 140, 180, 220];
  var sum = 0;
  for (int i = 0; i < 230; i++) {
    if (wantedCycles.contains(cpu.cycle)) {
      print('${cpu.cycle}\t${cpu.x}\t${cpu.crt}');
      sum += cpu.crt;
    }
    cpu.executeCycle();
  }
  print('Signal Sum: $sum');
}

void day10b() {
  final cpu = Cpu();
  final crt = Crt();
  File('data/cpu_instructions.txt')
      .readAsLinesSync()
      .map((l) => Instruction.fromLine(l))
      .forEach((i) => cpu.addInstruction(i));
  var buffer = '';
  for (int i = 0; i <= 240; i++) {
    if (crt.scanPosition == 0) {
      print(buffer);
      buffer = '';
    }
    final cursor = [cpu.x - 1, cpu.x, cpu.x + 1];
    if (cursor.contains(crt.scanPosition)) {
      buffer += '#';
    } else {
      buffer += '.';
    }
    crt.executeCycle();
    cpu.executeCycle();
  }
}
