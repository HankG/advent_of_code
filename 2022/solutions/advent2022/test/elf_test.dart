import 'package:advent2022/elf.dart';
import 'package:test/test.dart';

void main() {
  const elvesData = [
    '1000',
    '2000',
    '3000',
    '',
    '4000',
    '',
    '5000',
    '6000',
    '',
    '7000',
    '8000',
    '9000',
    '',
    '10000',
  ];

  test('Elf Total Food Test', () {
    final elf = Elf();
    const items = [1, 2, 3];
    const expectedTotal = 1 + 2 + 3;
    items.forEach((i) => elf.addFood(i));
    expect(elf.totalFood, equals(expectedTotal));
  });

  test('Elves Parse Test', () {
    final elves = parseElves(elvesData);
    elves.sort((e1, e2) => e1.totalFood.compareTo(e2.totalFood));
    expect(elves.length, equals(4));
    expect(elves.first.totalFood, equals(4000));
    expect(elves.last.totalFood, equals(24000));
  });
}
