import 'package:advent2022/tree_visibility.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  final gridLines = [
    '30373',
    '25512',
    '65332',
    '33549',
    '35390',
  ];

  test('test tree parsing', () {
    final grid = TreeGrid.fromLines(gridLines);
    expect(grid.rowCount, equals(5));
    expect(grid.colCount, equals(5));
    expect(grid.getHeight(0, 0), equals(3));
    expect(grid.getHeight(0, 1), equals(0));
    expect(grid.getHeight(1, 0), equals(2));
    expect(grid.getHeight(1, 1), equals(5));
  });

  test('test visibility calc', () {
    final grid = TreeGrid.fromLines(gridLines);
    final visibility = grid.calcVisibilityGrid();
    expect(visibility[0][0], equals(true));
    expect(visibility[1][1], equals(true));
    expect(visibility[2][2], equals(false));
    expect(grid.totalVisibleCount(), equals(21));
  });

  test('test scenic score calc', () {
    final grid = TreeGrid.fromLines(gridLines);
    expect(grid.calcScenicScore(1, 2), equals(4));
    expect(grid.calcScenicScore(3, 2), equals(8));
  });
}
