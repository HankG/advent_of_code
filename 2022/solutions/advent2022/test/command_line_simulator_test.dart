import 'package:advent2022/command_line_simulator.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  final lines = [
    '\$ cd /',
    '\$ ls',
    'dir a',
    '14848514 b.txt',
    '8504156 c.dat',
    'dir d',
    '\$ cd a',
    '\$ ls',
    'dir e',
    '29116 f',
    '2557 g',
    '62596 h.lst',
    '\$ cd e',
    '\$ ls',
    '584 i',
    '\$ cd ..',
    '\$ cd ..',
    '\$ cd d',
    '\$ ls',
    '4060174 j',
    '8033020 d.log',
    '5626152 d.ext',
    '7214296 k',
  ];
  test('CLI replay', () {
    final sim = replayOutputFile(lines);
    print('final output');
    expect(sim.rootDirectory.totalSize(), equals(48381165));
    expect(sim.rootDirectory.directories['a']!.totalSize(), equals(94853));
    expect(sim.rootDirectory.directories['a']!.directories['e']!.totalSize(),
        equals(584));
  });
}
