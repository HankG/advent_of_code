import 'package:advent2022/rucksack.dart';
import 'package:test/test.dart';

void main() {
  final rucksackData = [
    'vJrwpWtwJgWrhcsFMMfFFhFp',
    'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
    'PmmdzqPrVvPwwTWBwg',
    'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
    'ttgJtRGJQctTZtZT',
    'CrZsJsPPZsGzwwsLwLmpwMDw',
  ];

  test('rucksack total duplicate', () {
    final rucksacks = rucksackData.map((r) => Rucksack.fromInputString(r));
    final duplicates = rucksacks.map((r) => r.itemsInBothSum);
    final duplicatesSum =
        duplicates.reduce((total, rucksackTotal) => total + rucksackTotal);
    expect(duplicatesSum, equals(157));
  });

  test('rucksack partition', () {
    final rucksacks =
        rucksackData.map((r) => Rucksack.fromInputString(r)).toList();
    final groups = rucksacks.partition(3);
    expect(groups.length, equals(2));
    expect(groups.first.length, equals(3));
    expect(groups.last.length, equals(3));
  });

  test('rucksack partition', () {
    final rucksacks =
        rucksackData.map((r) => Rucksack.fromInputString(r)).toList();
    final groups = rucksacks.partition(3);
    expect(groups.length, equals(2));
    expect(groups.first.length, equals(3));
    expect(groups.last.length, equals(3));
  });

  test('rucksack candidateTokens', () {
    final rucksacks =
        rucksackData.map((r) => Rucksack.fromInputString(r)).toList();
    final groups = rucksacks.partition(3);
    expect(groups.first.groupBadgeCandidates().length, equals(1));
    expect(groups.first.groupBadgeCandidates().first, equals(18));
    expect(groups.last.groupBadgeCandidates().length, equals(1));
    expect(groups.last.groupBadgeCandidates().first, equals(52));
  });
}
