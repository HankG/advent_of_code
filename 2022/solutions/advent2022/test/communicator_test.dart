import 'package:advent2022/communicator.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  test('test find header start', () {
    // expect(findStart('a', StartType.header), equals(-1));
    // expect(findStart('ab', StartType.header), equals(-1));
    // expect(findStart('abc', StartType.header), equals(-1));
    // expect(findStart('abcd', StartType.header), equals(3));
    expect(findStart('abcad', StartType.header), equals(4));
    expect(findStart('mjqjpqmgbljsphdztnvjfqwrcgsmlb', StartType.header),
        equals(6));
    expect(
        findStart('bvwbjplbgvbhsrlpgdmjqwftvncz', StartType.header), equals(4));
    expect(
        findStart('nppdvjthqldpwncqszvftbrmjlhg', StartType.header), equals(5));
    expect(findStart('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', StartType.header),
        equals(9));
    expect(findStart('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', StartType.header),
        equals(10));
  });

  test('test find message start', () {
    expect(findStart('mjqjpqmgbljsphdztnvjfqwrcgsmlb', StartType.message),
        equals(18));
    expect(findStart('bvwbjplbgvbhsrlpgdmjqwftvncz', StartType.message),
        equals(22));
    expect(findStart('nppdvjthqldpwncqszvftbrmjlhg', StartType.message),
        equals(22));
    expect(findStart('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', StartType.message),
        equals(28));
    expect(findStart('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', StartType.message),
        equals(25));
  });
}
