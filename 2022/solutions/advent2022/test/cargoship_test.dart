import 'package:advent2022/cargoship.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  final allLines = [
    '    [D]    ',
    '[N] [C]    ',
    '[Z] [M] [P]',
    ' 1   2   3 ',
    '',
    'move 1 from 2 to 1',
    'move 3 from 1 to 3',
    'move 2 from 2 to 1',
    'move 1 from 1 to 2',
  ];

  test('stacks parsing test', () {
    final lines = allLines.sublist(0, 4);
    final stacks = parseInitialStacks(lines);
    expect(stacks.length, equals(3));
    expect(stacks.first.length, equals(2));
    expect(stacks.last.length, equals(1));
    expect(stacks.first.removeAt(0), equals('N'));
    expect(stacks.first.removeAt(0), equals('Z'));
    expect(stacks.last.removeAt(0), equals('P'));
  });

  test('move parsing test', () {
    final line = 'move 3 from 2 to 1';
    final move = MoveInstruction.fromLine(line);
    expect(move.fromStack, equals(2));
    expect(move.toStack, equals(1));
    expect(move.count, equals(3));
  });

  test('At once false Stack manipulating test', () {
    final ships = [<String>[], <String>[], <String>[]];
    ships.first.insert(0, 'N');
    ships.first.insert(0, 'Z');
    ships.first.insert(0, 'A');
    ships.last.insert(0, 'X');
    final move = MoveInstruction(fromStack: 1, toStack: 3, count: 2);
    executeMoveInstruction(ships, move, false);
    expect(ships.first.length, equals(1));
    expect(ships.first.removeAt(0), equals('N'));
    expect(ships.last.removeAt(0), equals('Z'));
    expect(ships.last.removeAt(0), equals('A'));
    expect(ships.last.removeAt(0), equals('X'));
  });

  test('At once Stack manipulating test', () {
    final ships = [<String>[], <String>[], <String>[]];
    ships.first.insert(0, 'N');
    ships.first.insert(0, 'Z');
    ships.first.insert(0, 'A');
    ships.last.insert(0, 'X');
    final move = MoveInstruction(fromStack: 1, toStack: 3, count: 2);
    executeMoveInstruction(ships, move, true);
    expect(ships.first.length, equals(1));
    expect(ships.first.removeAt(0), equals('N'));
    expect(ships.last.removeAt(0), equals('A'));
    expect(ships.last.removeAt(0), equals('Z'));
    expect(ships.last.removeAt(0), equals('X'));
  });
}
