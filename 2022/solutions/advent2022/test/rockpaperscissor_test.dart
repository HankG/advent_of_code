import 'package:advent2022/rockpaperscissor.dart';
import 'package:test/test.dart';

void main() {
  group('Rock scores', () {
    test('Opponent Rock', () {
      expect(
        RpsGame(you: RpsHand.rock, opponent: RpsHand.rock).playRound(),
        equals(1 + drawScore),
      );
    });
    test('Opponent Scissor', () {
      expect(
        RpsGame(you: RpsHand.rock, opponent: RpsHand.scissors).playRound(),
        equals(1 + winScore),
      );
    });
    test('Opponent Paper', () {
      expect(
        RpsGame(you: RpsHand.rock, opponent: RpsHand.paper).playRound(),
        equals(1 + lossScore),
      );
    });
  });

  group('Paper scores', () {
    test('Opponent Rock', () {
      expect(
        RpsGame(you: RpsHand.paper, opponent: RpsHand.rock).playRound(),
        equals(2 + winScore),
      );
    });
    test('Opponent Scissor', () {
      expect(
        RpsGame(you: RpsHand.paper, opponent: RpsHand.scissors).playRound(),
        equals(2 + lossScore),
      );
    });
    test('Opponent Paper', () {
      expect(
        RpsGame(you: RpsHand.paper, opponent: RpsHand.paper).playRound(),
        equals(2 + drawScore),
      );
    });
  });

  group('Scissors scores', () {
    test('Opponent Rock', () {
      expect(
        RpsGame(you: RpsHand.scissors, opponent: RpsHand.rock).playRound(),
        equals(3 + lossScore),
      );
    });
    test('Opponent Scissor', () {
      expect(
        RpsGame(you: RpsHand.scissors, opponent: RpsHand.scissors).playRound(),
        equals(3 + drawScore),
      );
    });
    test('Opponent Paper', () {
      expect(
        RpsGame(you: RpsHand.scissors, opponent: RpsHand.paper).playRound(),
        equals(3 + winScore),
      );
    });
  });

  test('Test Game1 Parser', () {
    final gamesText = [
      'A Y',
      'B X',
      'C Z',
    ];
    final games = parseGame1(gamesText);
    final totalScore =
        games.map((g) => g.playRound()).reduce((total, game) => total + game);
    expect(totalScore, equals(15));
  });

  test('Test Game2 Parser', () {
    final gamesText = [
      'A Y',
      'B X',
      'C Z',
    ];
    final games = parseGame2(gamesText);
    games.forEach(print);
    final totalScore =
        games.map((g) => g.playRound()).reduce((total, game) => total + game);
    expect(totalScore, equals(12));
  });
}
