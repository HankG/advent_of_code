import 'package:advent2022/rope.dart';
import 'package:test/scaffolding.dart';

void main() {
  test('move test', () {
    final rope = Rope();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    rope.moveHeadRight();
    print(drawRope(rope));
    print('');
    rope.moveHeadUp();
    rope.moveHeadUp();
    rope.moveHeadUp();
    rope.moveHeadUp();
    rope.moveHeadUp();
    rope.moveHeadUp();
    rope.moveHeadUp();
    rope.moveHeadUp();
    print(drawRope(rope));
    print('');
    rope.moveHeadLeft();
    rope.moveHeadLeft();
    rope.moveHeadLeft();
    rope.moveHeadLeft();
    rope.moveHeadLeft();
    rope.moveHeadLeft();
    rope.moveHeadLeft();
    rope.moveHeadLeft();
    print(drawRope(rope));
    print('');
    rope.moveHeadDown();
    rope.moveHeadDown();
    rope.moveHeadDown();
    print(drawRope(rope));
    print('');
    rope.tailHistory.forEach(print);
  });
}
