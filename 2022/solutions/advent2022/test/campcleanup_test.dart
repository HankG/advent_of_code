import 'package:advent2022/campcleanup.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  test('SectionList factory fromLine', () {
    final lines = [
      '2-4,6-8',
      '2-3,4-5',
      '5-7,7-9',
      '2-8,3-7',
      '6-6,4-6',
      '2-6,4-8',
    ];
    final sectionsListsPairs =
        lines.map((l) => SectionList.fromCsvLine(l)).toList();
    expect(sectionsListsPairs.length, equals(lines.length));
    expect(sectionsListsPairs.first.first.start, equals(2));
    expect(sectionsListsPairs.first.first.stop, equals(4));
    expect(sectionsListsPairs.first.last.start, equals(6));
    expect(sectionsListsPairs.last.first.start, equals(2));
    expect(sectionsListsPairs.last.first.stop, equals(6));
    expect(sectionsListsPairs.last.last.start, equals(4));
    expect(sectionsListsPairs.last.last.stop, equals(8));
  });

  test('SectionList contains', () {
    expect(
        SectionList(
          start: 4,
          stop: 6,
        ).contains(
          SectionList(
            start: 6,
            stop: 6,
          ),
        ),
        true);
    expect(
        SectionList(
          start: 2,
          stop: 8,
        ).contains(
          SectionList(
            start: 3,
            stop: 7,
          ),
        ),
        true);

    expect(
        SectionList(
          start: 5,
          stop: 7,
        ).contains(
          SectionList(
            start: 7,
            stop: 9,
          ),
        ),
        false);

    expect(
        SectionList(
          start: 2,
          stop: 4,
        ).contains(
          SectionList(
            start: 6,
            stop: 8,
          ),
        ),
        false);

    expect(
        SectionList(
          start: 9,
          stop: 10,
        ).contains(
          SectionList(
            start: 10,
            stop: 88,
          ),
        ),
        false);

    expect(
        SectionList(
          start: 10,
          stop: 88,
        ).contains(
          SectionList(
            start: 9,
            stop: 10,
          ),
        ),
        false);
  });

  test('SectionList overlap', () {
    expect(
        SectionList(
          start: 4,
          stop: 6,
        ).overlaps(
          SectionList(
            start: 3,
            stop: 7,
          ),
        ),
        true);

    expect(
        SectionList(
          start: 4,
          stop: 6,
        ).overlaps(
          SectionList(
            start: 7,
            stop: 8,
          ),
        ),
        false);
  });
}
