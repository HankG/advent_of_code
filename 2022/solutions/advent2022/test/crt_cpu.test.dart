import 'package:advent2022/crt_cpu.dart';
import 'package:test/test.dart';

void main() {
  test('Test parser', () {
    final noop = Instruction.fromLine('noop');
    expect(noop.type, equals(InstructionType.noop));
    expect(noop.increment, equals(0));
    expect(noop.generateAdderCycles(), equals([0]));
    final addx = Instruction.fromLine('addx 5');
    expect(addx.type, equals(InstructionType.addx));
    expect(addx.increment, equals(5));
    expect(addx.generateAdderCycles(), equals([0, 5]));
  });

  test('Test basic CPU ops', () {
    final cpu = Cpu();
    expect(cpu.x, equals(1));
    cpu.addInstruction(Instruction(InstructionType.noop));
    cpu.executeCycle();
    expect(cpu.x, equals(1));
    cpu.addInstruction(Instruction(InstructionType.addx, increment: 3));
    cpu.executeCycle();
    expect(cpu.x, equals(1));
    cpu.executeCycle();
    expect(cpu.x, equals(4));
    cpu.addInstruction(Instruction(InstructionType.addx, increment: -5));
    cpu.executeCycle();
    expect(cpu.x, equals(4));
    cpu.executeCycle();
    expect(cpu.x, equals(-1));
  });

  test('Test complex ops', () {
    final cpu = Cpu();
    final ops = instructions.map((i) => Instruction.fromLine(i)).toList();
    ops.forEach((o) => cpu.addInstruction(o));
    print('Cycle\tX\tCRT');
    final wantedCycles = [20, 60, 100, 140, 180, 220];
    var sum = 0;
    for (int i = 0; i < 230; i++) {
      if (wantedCycles.contains(cpu.cycle)) {
        print('${cpu.cycle}\t${cpu.x}\t${cpu.crt}');
        sum += cpu.crt;
      }
      cpu.executeCycle();
    }
    print('Signal Sum: $sum');
  });

  test('Test display ops', () {
    final cpu = Cpu();
    final crt = Crt();
    final ops = instructions.map((i) => Instruction.fromLine(i)).toList();
    ops.forEach((o) => cpu.addInstruction(o));
    var buffer = '';
    for (int i = 0; i < 300; i++) {
      if (crt.scanPosition == 0) {
        print(buffer);
        buffer = '';
      }
      final cursor = [cpu.x - 1, cpu.x, cpu.x + 1];
      if (cursor.contains(crt.scanPosition)) {
        buffer += '#';
      } else {
        buffer += '.';
      }
      crt.executeCycle();
      cpu.executeCycle();
    }
  });
}

final instructions = [
  'addx 15',
  'addx -11',
  'addx 6',
  'addx -3',
  'addx 5',
  'addx -1',
  'addx -8',
  'addx 13',
  'addx 4',
  'noop',
  'addx -1',
  'addx 5',
  'addx -1',
  'addx 5',
  'addx -1',
  'addx 5',
  'addx -1',
  'addx 5',
  'addx -1',
  'addx -35',
  'addx 1',
  'addx 24',
  'addx -19',
  'addx 1',
  'addx 16',
  'addx -11',
  'noop',
  'noop',
  'addx 21',
  'addx -15',
  'noop',
  'noop',
  'addx -3',
  'addx 9',
  'addx 1',
  'addx -3',
  'addx 8',
  'addx 1',
  'addx 5',
  'noop',
  'noop',
  'noop',
  'noop',
  'noop',
  'addx -36',
  'noop',
  'addx 1',
  'addx 7',
  'noop',
  'noop',
  'noop',
  'addx 2',
  'addx 6',
  'noop',
  'noop',
  'noop',
  'noop',
  'noop',
  'addx 1',
  'noop',
  'noop',
  'addx 7',
  'addx 1',
  'noop',
  'addx -13',
  'addx 13',
  'addx 7',
  'noop',
  'addx 1',
  'addx -33',
  'noop',
  'noop',
  'noop',
  'addx 2',
  'noop',
  'noop',
  'noop',
  'addx 8',
  'noop',
  'addx -1',
  'addx 2',
  'addx 1',
  'noop',
  'addx 17',
  'addx -9',
  'addx 1',
  'addx 1',
  'addx -3',
  'addx 11',
  'noop',
  'noop',
  'addx 1',
  'noop',
  'addx 1',
  'noop',
  'noop',
  'addx -13',
  'addx -19',
  'addx 1',
  'addx 3',
  'addx 26',
  'addx -30',
  'addx 12',
  'addx -1',
  'addx 3',
  'addx 1',
  'noop',
  'noop',
  'noop',
  'addx -9',
  'addx 18',
  'addx 1',
  'addx 2',
  'noop',
  'noop',
  'addx 9',
  'noop',
  'noop',
  'noop',
  'addx -1',
  'addx 2',
  'addx -37',
  'addx 1',
  'addx 3',
  'noop',
  'addx 15',
  'addx -21',
  'addx 22',
  'addx -6',
  'addx 1',
  'noop',
  'addx 2',
  'addx 1',
  'noop',
  'addx -10',
  'noop',
  'noop',
  'addx 20',
  'addx 1',
  'addx 2',
  'addx 2',
  'addx -6',
  'addx -11',
  'noop',
  'noop',
  'noop'
];
