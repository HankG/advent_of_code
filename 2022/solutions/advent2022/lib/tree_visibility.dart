class TreeGrid {
  final _grid = <List<int>>[];

  TreeGrid();

  int get rowCount => _grid.length;

  int get colCount => _grid.isEmpty ? 0 : _grid.first.length;

  int getHeight(int row, int column) => _grid[row][column];

  bool isVisible(int row, int column) {
    if (row == 0 || row == _grid.length - 1) {
      return true;
    }

    if (column == 0 || column == _grid.first.length - 1) {
      return true;
    }

    final myHeight = _grid[row][column];
    bool leftVisible = true;
    bool rightVisible = true;
    bool topVisible = true;
    bool bottomVisible = true;

    for (int j = 0; j < _grid[row].length; j++) {
      if (j == column) {
        continue;
      }
      final visible = myHeight > _grid[row][j];
      if (!visible && j < column) {
        leftVisible = false;
      }

      if (!visible && j > column) {
        rightVisible = false;
      }
    }

    for (int i = 0; i < _grid.length; i++) {
      if (i == row) {
        continue;
      }
      final visible = myHeight > _grid[i][column];
      if (!visible && i < row) {
        topVisible = false;
      }

      if (!visible && i > row) {
        bottomVisible = false;
      }
    }

    return topVisible || bottomVisible || leftVisible || rightVisible;
  }

  int calcScenicScore(int row, int column) {
    final myHeight = _grid[row][column];

    int leftSpan = 0;
    for (int j = column - 1; j >= 0; j--) {
      leftSpan++;
      if (_grid[row][j] >= myHeight) {
        break;
      }
    }

    int rightSpan = 0;
    for (int j = column + 1; j < _grid[row].length; j++) {
      rightSpan++;
      if (_grid[row][j] >= myHeight) {
        break;
      }
    }

    int topSpan = 0;
    for (int i = row - 1; i >= 0; i--) {
      topSpan++;
      if (_grid[i][column] >= myHeight) {
        break;
      }
    }

    int bottomSpan = 0;
    for (int i = row + 1; i < _grid.length; i++) {
      bottomSpan++;
      if (_grid[i][column] >= myHeight) {
        break;
      }
    }

    return topSpan * bottomSpan * leftSpan * rightSpan;
  }

  List<List<bool>> calcVisibilityGrid() {
    final result = <List<bool>>[];
    for (int i = 0; i < _grid.length; i++) {
      final resultRow = <bool>[];
      result.add(resultRow);
      for (int j = 0; j < _grid[i].length; j++) {
        resultRow.add(isVisible(i, j));
      }
    }
    return result;
  }

  List<List<int>> calcScenicScoreGrid() {
    final result = <List<int>>[];
    for (int i = 0; i < _grid.length; i++) {
      final resultRow = <int>[];
      result.add(resultRow);
      for (int j = 0; j < _grid[i].length; j++) {
        resultRow.add(calcScenicScore(i, j));
      }
    }
    return result;
  }

  int totalVisibleCount() {
    final visibility = calcVisibilityGrid();
    int count = 0;
    for (var i = 0; i < visibility.length; i++) {
      for (var j = 0; j < visibility[i].length; j++) {
        if (visibility[i][j] == true) {
          count++;
        }
      }
    }

    return count;
  }

  factory TreeGrid.fromLines(List<String> lines) {
    final grid = TreeGrid();
    for (final line in lines) {
      final row = <int>[];
      grid._grid.add(row);
      for (var column = 0; column < line.length; column++) {
        final height = int.parse(line[column]);
        row.add(height);
      }
    }
    return grid;
  }

  @override
  String toString() {
    if (_grid.isEmpty) {
      return 'Empty grid';
    }
    final gridText = _grid.map((row) => row.join(' ')).join('\n');
    return '$gridText\nRows: ${_grid.length}, Columns: ${_grid.first.length}';
  }

  String toVisibilityGridString() {
    if (_grid.isEmpty) {
      return 'Empty grid';
    }
    final visibilityGrid = calcVisibilityGrid();
    final gridText = visibilityGrid.map((row) => row.join(' ')).join('\n');
    return '$gridText\nRows: ${_grid.length}, Columns: ${_grid.first.length}';
  }
}
