const lossScore = 0;
const drawScore = 3;
const winScore = 6;

const rockBonus = 1;
const paperBonus = 2;
const scissorBonus = 3;

enum RpsHand {
  paper,
  scissors,
  rock,
}

class RpsGame {
  final RpsHand you;
  final RpsHand opponent;

  RpsGame({required this.you, required this.opponent});

  int playRound() {
    late final int bonus;
    switch (you) {
      case RpsHand.paper:
        bonus = paperBonus;
        break;
      case RpsHand.scissors:
        bonus = scissorBonus;
        break;
      case RpsHand.rock:
        bonus = rockBonus;
        break;
    }

    if (you == opponent) {
      return bonus + drawScore;
    }

    switch (you) {
      case RpsHand.paper:
        return bonus + (opponent == RpsHand.rock ? winScore : lossScore);
      case RpsHand.scissors:
        return bonus + (opponent == RpsHand.paper ? winScore : lossScore);
      case RpsHand.rock:
        return bonus + (opponent == RpsHand.scissors ? winScore : lossScore);
    }
  }

  @override
  String toString() {
    return 'RpsGame{you: $you, opponent: $opponent, score: ${playRound()}';
  }
}

List<RpsGame> parseGame1(List<String> lines) {
  final games = <RpsGame>[];

  for (final line in lines) {
    final players = line.split(RegExp('\\s+'));
    final opponent = players[0];
    final you = players[1];
    late final RpsHand opponentHand;
    if (opponent == 'A') {
      opponentHand = RpsHand.rock;
    } else if (opponent == 'B') {
      opponentHand = RpsHand.paper;
    } else if (opponent == 'C') {
      opponentHand = RpsHand.scissors;
    } else {
      throw ArgumentError('Unknown opponent type: |$opponent|');
    }

    late final RpsHand yourHand;
    if (you == 'X') {
      yourHand = RpsHand.rock;
    } else if (you == 'Y') {
      yourHand = RpsHand.paper;
    } else if (you == 'Z') {
      yourHand = RpsHand.scissors;
    } else {
      throw ArgumentError('Unknown your hand type: |$you|');
    }

    games.add(RpsGame(you: yourHand, opponent: opponentHand));
  }

  return games;
}

List<RpsGame> parseGame2(List<String> lines) {
  final games = <RpsGame>[];

  for (final line in lines) {
    final players = line.split(RegExp('\\s+'));
    final opponent = players[0];
    final you = players[1];
    late final RpsHand opponentHand;
    if (opponent == 'A') {
      opponentHand = RpsHand.rock;
    } else if (opponent == 'B') {
      opponentHand = RpsHand.paper;
    } else if (opponent == 'C') {
      opponentHand = RpsHand.scissors;
    } else {
      throw ArgumentError('Unknown opponent type: |$opponent|');
    }

    late final RpsHand yourHand;
    if (you == 'X') {
      yourHand = generateLoser(opponentHand);
    } else if (you == 'Y') {
      yourHand = opponentHand;
    } else if (you == 'Z') {
      yourHand = generateWinner(opponentHand);
    } else {
      throw ArgumentError('Unknown your hand type: |$you|');
    }

    games.add(RpsGame(you: yourHand, opponent: opponentHand));
  }

  return games;
}

RpsHand generateLoser(RpsHand opponenent) {
  switch (opponenent) {
    case RpsHand.paper:
      return RpsHand.rock;
    case RpsHand.scissors:
      return RpsHand.paper;
    case RpsHand.rock:
      return RpsHand.scissors;
  }
}

RpsHand generateWinner(RpsHand opponenent) {
  switch (opponenent) {
    case RpsHand.paper:
      return RpsHand.scissors;
    case RpsHand.scissors:
      return RpsHand.rock;
    case RpsHand.rock:
      return RpsHand.paper;
  }
}
