class MoveInstruction {
  final int fromStack;
  final int toStack;
  final int count;

  MoveInstruction({
    required this.fromStack,
    required this.toStack,
    required this.count,
  });

  factory MoveInstruction.fromLine(String line) {
    final instructions = line.split(RegExp('\\s+'));
    final count = int.parse(instructions[1]);
    final from = int.parse(instructions[3]);
    final to = int.parse(instructions[5]);
    return MoveInstruction(fromStack: from, toStack: to, count: count);
  }
}

void executeMoveInstruction(
    List<List<String>> ships, MoveInstruction move, bool atOnce) {
  final from = ships[move.fromStack - 1];
  final to = ships[move.toStack - 1];

  if (atOnce) {
    final items = from.sublist(0, move.count);
    to.insertAll(0, items);
    from.removeRange(0, move.count);
  } else {
    for (var steps = 0; steps < move.count; steps++) {
      to.insert(0, from.removeAt(0));
    }
  }
}

List<List<String>> parseInitialStacks(List<String> lines) {
  final result = <List<String>>[];

  final columns = <int>[];
  final header = lines.last;
  for (var i = 0; i < header.length; i++) {
    if (int.tryParse(header[i]) != null) {
      columns.add(i);
      result.add(<String>[]);
    }
  }

  for (var lineIndex = lines.length - 2; lineIndex >= 0; lineIndex--) {
    final line = lines[lineIndex];
    for (var stackIndex = 0; stackIndex < result.length; stackIndex++) {
      final column = columns[stackIndex];
      if (line.length < column) {
        continue;
      }
      final item = line[column].trim();
      if (item.isNotEmpty) {
        result[stackIndex].insert(0, item);
      }
    }
  }

  return result;
}
