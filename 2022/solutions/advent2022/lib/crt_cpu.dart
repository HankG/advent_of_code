enum InstructionType {
  addx,
  noop,
}

class Instruction {
  final InstructionType type;
  final int increment;

  Instruction(this.type, {this.increment = 0});

  List<int> generateAdderCycles() {
    switch (type) {
      case InstructionType.addx:
        return [0, increment];
      case InstructionType.noop:
        return [0];
    }
  }

  factory Instruction.fromLine(String line) {
    final elements = line.split(RegExp('\\s+'));
    final command = elements[0];
    if (command == 'noop') {
      return Instruction(InstructionType.noop);
    } else if (command == 'addx') {
      final int increment = int.parse(elements[1]);
      return Instruction(InstructionType.addx, increment: increment);
    }

    throw ArgumentError('Unknown command type: $command');
  }
}

class Cpu {
  var _cycle = 1;
  var _x = 1;
  final instructionPipeline = <int>[];

  int get x => _x;

  int get crt => x * _cycle;

  int get cycle => _cycle;

  void addInstruction(Instruction instruction) {
    instructionPipeline.addAll(instruction.generateAdderCycles());
  }

  void executeCycle() {
    if (instructionPipeline.isEmpty) {
      return;
    }
    final xIncrement = instructionPipeline.removeAt(0);
    _x += xIncrement;
    _cycle++;
  }
}

class Crt {
  var _scanPosition = 0;

  int get scanPosition => _scanPosition;

  void executeCycle() {
    if (_scanPosition == 39) {
      _scanPosition = 0;
    } else {
      _scanPosition++;
    }
  }
}
