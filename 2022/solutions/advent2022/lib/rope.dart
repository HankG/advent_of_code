import 'dart:math';

class Position {
  final int x;
  final int y;

  const Position({required this.x, required this.y});

  Position move({int dx = 0, int dy = 0}) => Position(x: x + dx, y: y + dy);

  Position delta(Position other) => Position(x: x - other.x, y: y - other.y);

  double get magnitude => sqrt(x * x + y * y);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Position &&
          runtimeType == other.runtimeType &&
          x == other.x &&
          y == other.y;

  @override
  int get hashCode => x.hashCode ^ y.hashCode;

  @override
  String toString() {
    return '[$x, $y]';
  }
}

class Rope {
  var segments = List.generate(10, (index) => Position(x: 0, y: 0));
  var tailHistory = {Position(x: 0, y: 0)};

  void moveHeadRight() {
    moveHead(dx: 1);
  }

  void moveHeadLeft() {
    moveHead(dx: -1);
  }

  void moveHeadUp() {
    moveHead(dy: 1);
  }

  void moveHeadDown() {
    moveHead(dy: -1);
  }

  void moveHead({int dx = 0, int dy = 0}) {
    var previousDelta = Position(x: dx, y: dy);
    for (var i = 0; i < segments.length; i++) {
      if (i == 0) {
        segments[i] = segments[i].move(dx: dx, dy: dy);
      } else {
        final delta = calculateSegmentUpdate(
            first: segments[i - 1],
            second: segments[i],
            dx: previousDelta.x,
            dy: previousDelta.y);
        segments[i] = segments[i].move(dx: delta.x, dy: delta.y);
        previousDelta = delta;
        if (i == segments.length - 1) {
          tailHistory.add(segments[i]);
        }
      }
    }
  }
}

Position calculateSegmentUpdate({
  required Position first,
  required Position second,
  int dx = 0,
  int dy = 0,
}) {
  final delta = first.delta(second);
  if (delta.magnitude <= 1) {
    return Position(x: 0, y: 0);
  }

  final diagonalMove = delta.x != 0 && delta.y != 0;
  if (!diagonalMove) {
    return Position(
        x: delta.x == 0
            ? 0
            : delta.x < 1
                ? -1
                : 1,
        y: delta.y == 0
            ? 0
            : delta.y < 1
                ? -1
                : 1);
  }

  if ((delta.x.abs() == 1 && delta.y.abs() == 1)) {
    return Position(x: 0, y: 0);
  }

  return Position(x: delta.x < 0 ? -1 : 1, y: delta.y < 0 ? -1 : 1);
}

String drawRope(Rope rope, {bool asTail = false}) {
  final lines = <String>[];
  var gridXMin = 0;
  var gridYMin = 0;
  var gridXMax = 0;
  var gridYMax = 0;
  for (final p in [...rope.segments, ...rope.tailHistory]) {
    gridXMin = min(gridXMin, p.x);
    gridYMin = min(gridYMin, p.y);
    gridXMax = max(gridXMax, p.x);
    gridYMax = max(gridYMax, p.y);
  }

  for (int y = gridYMin; y <= gridYMax; y++) {
    final newLinePieces = <String>[];
    for (int x = gridXMin; x <= gridXMax; x++) {
      var newChar =
          asTail ? _genTailChar(rope, x, y) : _genRopeChar(rope, x, y);
      if (newChar == '.' && x == 0 && y == 0) {
        newLinePieces.add('s');
      } else {
        newLinePieces.add(newChar);
      }
    }
    lines.insert(0, newLinePieces.join(' '));
  }
  return lines.join('\n');
}

String _genRopeChar(Rope rope, int x, int y) {
  for (int i = 0; i < rope.segments.length; i++) {
    final label = i == 0 ? 'H' : '$i';
    if (rope.segments[i].x == x && rope.segments[i].y == y) {
      return label;
    }
  }

  return '.';
}

String _genTailChar(Rope rope, int x, int y) {
  for (final t in rope.tailHistory) {
    if (t.x == x && t.y == y) {
      return '#';
    }
  }

  return '.';
}
