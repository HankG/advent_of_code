import 'package:result_monad/result_monad.dart';

class SimDirectory {
  final files = <SimFile>[];
  final directories = <String, SimDirectory>{};
  final SimDirectory? parent;
  final String name;

  SimDirectory(this.name, this.parent);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SimDirectory &&
          runtimeType == other.runtimeType &&
          name == other.name;

  int totalSize() {
    final fileTotalSizes = files.isEmpty
        ? 0
        : files.map((f) => f.size).reduce((total, fs) => total + fs);
    final dirTotalSizes = directories.isEmpty
        ? 0
        : directories.values
            .map((d) => d.totalSize())
            .reduce((total, ds) => total + ds);
    return fileTotalSizes + dirTotalSizes;
  }

  @override
  int get hashCode => name.hashCode;

  @override
  String toString() {
    return 'Directory{totalsize: ${totalSize()}, parent: ${parent?.name}, name: $name,files: $files, directories: $directories,}';
  }
}

class SimFile {
  final int size;
  final String name;

  const SimFile({required this.name, required this.size});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SimFile &&
          runtimeType == other.runtimeType &&
          name == other.name;

  @override
  int get hashCode => name.hashCode;

  @override
  String toString() {
    return 'File{size: $size, name: $name}';
  }
}

class CommandlineSimulator {
  final rootDirectory = SimDirectory('/', null);
  final allDirs = <SimDirectory>[];
  late SimDirectory currentDirectory;

  CommandlineSimulator() {
    currentDirectory = rootDirectory;
  }

  Result<SimDirectory, String> executeUpDir() {
    currentDirectory = currentDirectory.parent ?? rootDirectory;
    return Result.ok(currentDirectory);
  }

  Result<SimDirectory, String> executeCdInto(String name) {
    if (currentDirectory.directories.containsKey(name)) {
      ;
      currentDirectory = currentDirectory.directories[name]!;
      return Result.ok(currentDirectory);
    }

    return Result.error(
        'Directory not found: $name in ${currentDirectory.name}');
  }

  Result<SimDirectory, String> executeCdToRoot() {
    currentDirectory = rootDirectory;
    return Result.ok(currentDirectory);
  }

  Result<SimDirectory, String> populateDirFromLs(String line) {
    final elements = line.split(RegExp('\\s+'));
    final name = elements[1];
    if (elements.length != 2) {
      return Result.error("ls output line didn't have two elements: $line");
    }
    if (elements.first == 'dir') {
      final newDir = SimDirectory(name, currentDirectory);
      allDirs.add(newDir);
      currentDirectory.directories[name] = newDir;
    } else {
      final size = int.parse(elements.first);
      currentDirectory.files.add(SimFile(name: name, size: size));
    }

    return Result.ok(currentDirectory);
  }
}

CommandlineSimulator replayOutputFile(List<String> lines) {
  final sim = CommandlineSimulator();
  bool inDirListMode = false;
  for (final line in lines) {
    if (line.startsWith('\$')) {
      final elements = line.split(RegExp('\\s+'));
      final command = elements[1];
      if (command == 'ls') {
        inDirListMode = true;
      } else if (command == 'cd') {
        inDirListMode = false;
        final name = elements[2];
        if (name == '..') {
          sim.executeUpDir();
        } else if (name == '/') {
          sim.executeCdToRoot();
        } else {
          sim.executeCdInto(name);
        }
      }
    } else {
      if (inDirListMode) {
        sim.populateDirFromLs(line);
      } else {
        print('Got output line that was not a command but not in ls mode');
      }
    }
  }
  return sim;
}
