class Elf {
  final food = <int>[];

  void addFood(int item) => food.add(item);

  int get totalFood => food.reduce((total, item) => total + item);

  @override
  String toString() {
    return 'Elf{totalFood: $totalFood, food: $food}';
  }
}

List<Elf> parseElves(List<String> lines) {
  final elves = <Elf>[];
  var currentElf = Elf();
  for (final line in lines) {
    if (line.isEmpty) {
      currentElf = Elf();
      elves.add(currentElf);
      continue;
    }

    final food = int.parse(line);
    currentElf.addFood(food);
  }

  return elves;
}
