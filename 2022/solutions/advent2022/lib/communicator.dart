enum StartType { header, message }

int findStart(String stream, StartType type) {
  final necessaryLength = type == StartType.header ? 4 : 14;
  if (stream.length < necessaryLength) {
    return -1;
  }

  for (int i = necessaryLength - 1; i < stream.length; i++) {
    final subIndex = i - necessaryLength + 1;
    final subString = stream.substring(subIndex, subIndex + necessaryLength);
    final chars = Set.of(subString.codeUnits);
    if (chars.length == necessaryLength) {
      return i;
    }
  }

  return -1;
}
