class SectionList {
  final int start;
  final int stop;

  SectionList({required this.start, required this.stop});

  factory SectionList.fromline(String line) {
    final numbers = line.trim().split('-');
    final start = int.parse(numbers.first);
    final stop = int.parse(numbers.last);
    return SectionList(start: start, stop: stop);
  }

  static List<SectionList> fromCsvLine(String line) =>
      line.trim().split(',').map((l) => SectionList.fromline(l)).toList();

  bool contains(SectionList other) =>
      start <= other.start && stop >= other.stop;

  bool overlaps(SectionList other) =>
      (other.start >= start && other.start <= stop) ||
      (other.stop >= start && other.stop <= stop) ||
      (start >= other.start && start <= other.stop) ||
      (stop >= other.start && stop <= other.stop);

  @override
  String toString() {
    return 'SectionList{start: $start, stop: $stop}';
  }
}
