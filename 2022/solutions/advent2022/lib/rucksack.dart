class Rucksack {
  final List<int> compartment1;
  final List<int> compartment2;

  Rucksack({required this.compartment1, required this.compartment2});

  factory Rucksack.fromInputString(String init) {
    if (init.length.isOdd) {
      throw ArgumentError('Init string must have an even length');
    }
    final c1 = init.substring(0, init.length ~/ 2);
    final c2 = init.substring(init.length ~/ 2);

    final compartment1 = c1.codeUnits.map(_charToPriority).toList();
    final compartment2 = c2.codeUnits.map(_charToPriority).toList();
    return Rucksack(
      compartment1: compartment1,
      compartment2: compartment2,
    );
  }

  List<int> get itemsInBoth =>
      compartment1.where((i) => compartment2.contains(i)).toSet().toList();

  List<int> get uniqueItems => <int>{...compartment1, ...compartment2}.toList();

  int get itemsInBothSum => itemsInBoth.reduce((sum, i) => sum + i);
}

extension RuckSackListExtensions on List<Rucksack> {
  List<List<Rucksack>> partition(int maxSize) {
    final result = <List<Rucksack>>[];
    var current = <Rucksack>[];
    result.add(current);
    for (var i = 0; i < length; i++) {
      if (current.length >= maxSize) {
        current = <Rucksack>[];
        result.add(current);
      }
      current.add(this[i]);
    }
    return result;
  }

  List<int> groupBadgeCandidates() {
    final counters = <int, int>{};
    final items = map((e) => e.uniqueItems)
        .reduce((allItems, newItems) => allItems..addAll(newItems))
        .toList();
    for (final i in items) {
      counters.putIfAbsent(i, () => 0);
      counters[i] = counters[i]! + 1;
    }

    counters.removeWhere((key, value) => value != length);
    return counters.keys.toList();
  }

  List<int> commonItems() {
    final result = <int>{};
    for (final r in this) {
      result.addAll(r.itemsInBoth);
    }
    return result.toList();
  }
}

int _charToPriority(int x) {
  if (x >= 97 && x <= 122) {
    return x - 96;
  }

  if (x >= 65 && x <= 90) {
    return x - 38;
  }

  throw ArgumentError('Unknown character code: $x');
}
